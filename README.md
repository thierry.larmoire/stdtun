# stdtun

connect a tun or tap to stdin and stdout

## usage

put an ip to a local tap, connect to a distant tap with a bridge

```
test -e /tmp/fifo || mkfifo /tmp/fifo
./stdtun -p "local " -i 192.168.10.40 -m 255.255.255.0 </tmp/fifo | ssh user@site ./stdtun -p "distant " -b bridge >/tmp/fifo
```


