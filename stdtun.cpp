#if 0

proc build {} {
	global bin
	set src [info script]
	set bin [file normalize [file root $src]]
	exec >@stdout 2>@stderr g++ -g $src -o $bin
	catch { exec >@stdout 2>@stderr sudo -n chown 0:0 $bin }
	catch { exec >@stdout 2>@stderr sudo -n chmod +s $bin }
}

proc go { {debugLevel 0} } {
	build
	catch { exec >@stdout 2>/dev/null mkfifo pipe }
	global bin
	set p [open pipe {RDWR NONBLOCK}]
	exec 2>@stderr sudo $bin -d $debugLevel -p a:: -i 192.168.16.8 | sudo $bin -d $debugLevel -p b:: -b br0 >@$p <@$p
}

eval $argv

exit
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <linux/sockios.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <net/route.h>
#include <stdlib.h>

#define printe(fmt,args...) fprintf(stderr, "%s" fmt, debugPrefix, ##args)
#define debug(mask,fmt,args...) do { if(debugLevel&mask) fprintf(stderr, "%s" fmt, debugPrefix, ##args); } while(0)

#define arg() (argc--,*argv++)
#define arg_str(s) (argc? ((*(s)=     arg() ),1):0)
#define arg_int(i) (argc? ((*(i)=atoi(arg())),1):0)

unsigned int resolv(const char* host)
{
	unsigned int address;
	
	address = inet_addr(host);
	if(address != INADDR_NONE)
		return address;
	
	{
		hostent he_mem[1],*he;
		char buf[1024];
		int err;
		he=NULL;
		gethostbyname_r(host,he_mem,buf,sizeof(buf),&he,&err);
		if(!he)
			return INADDR_NONE;
		if(!he->h_addr_list[0])
			return INADDR_NONE;
		address = *((unsigned int*)he->h_addr_list[0]);
	}
	
	return address;
}

int main(int argc,char **argv)
{
	int fd;
	struct ifreq ifr;
	int status;
	const char *exe=NULL;
	const char *gateway=NULL;
	
#define OPTIONS \
	OPTION(str,p,debugPrefix,"") \
	OPTION(int,d,debugLevel,0) \
	OPTION(str,b,bridge,NULL) \
	OPTION(str,i,ip,NULL) \
	OPTION(str,m,mask,NULL) \
/**/

#define OPTION(type,short,name,def) \
	OPTION_##type(short,name,def)
#define OPTION_str(short,name,def) \
	const char *name=def; \
/**/
#define OPTION_int(short,name,def) \
	int name=def; \
/**/

OPTIONS
#undef OPTION
#undef OPTION_str
#undef OPTION_int
	
	exe=arg();
	for(const char*cmd;arg_str(&cmd);)
	{
		debug(4,"%s\n",cmd);
		if(0);
#define OPTION(type,short,name,def) \
		else if(0==strcmp("-" #short,cmd)) \
		{ \
			arg_##type(&name); \
		} \
/**/
OPTIONS
#undef OPTION
		else
		{
			printe("%s ",exe);
#define OPTION(type,short,name,def) \
	OPTION_##type(short,name,def)
#define OPTION_str(short,name,def) \
			printe(" -%s %s(%s)\n",#short,#name,(const char*) def?def:"not set"); \
/**/
#define OPTION_int(short,name,def) \
			printe(" -%s %s(%d)\n",#short,#name,def); \
/**/
OPTIONS
#undef OPTION
#undef OPTION_str
#undef OPTION_int
		}
	}
	
	fd=open("/dev/net/tun", O_CLOEXEC|O_RDWR);
	if(fd<0)
	{
		printe("!open\n");
		return 1;
	}
	debug(4,"fd %d\n",fd);
	
	memset(&ifr, 0, sizeof(ifr));
	ifr.ifr_flags = IFF_TAP|IFF_NO_PI;
	status=ioctl(fd, TUNSETIFF, (void *) &ifr);
	if(status<0)
	{
		printe("!setiff\n");
		close(fd);
		return 1;
	}
	printe("ifname %s\n",ifr.ifr_name);
	char ifname[strlen(ifr.ifr_name)+1];
	strcpy(ifname,ifr.ifr_name);
	
	{
		int fd;
		memset(&ifr, 0, sizeof(ifr));
		ifr.ifr_addr.sa_family = AF_INET;
		strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
		fd = socket(AF_INET, SOCK_DGRAM, 0);
		if(fd<0)
		{
			printe("!socket\n");
		}
		if(ioctl(fd, SIOCGIFFLAGS, &ifr)<0)
			printe("!SIOCGIFFLAGS %d\n",errno);
		ifr.ifr_flags |= IFF_UP;
		if(ioctl(fd, SIOCSIFFLAGS, &ifr)<0)
			printe("!SIOCSIFFLAGS\n");
		close(fd);
	}

	if(bridge)
	{
		int fd;
		fd=socket(AF_LOCAL, SOCK_STREAM, 0);
		if(fd<0)
		{
			printe("!socket\n");
		}
		unsigned int ifindex;
		ifindex=if_nametoindex(ifname);
		strncpy(ifr.ifr_name, bridge, IFNAMSIZ);
		ifr.ifr_ifindex = ifindex;
		status=ioctl(fd, SIOCBRADDIF, &ifr);
		if(status<0)
		{
			printe("!SIOCBRADDIF\n");
		}
		close(fd);
	}
	
	if(ip)
	{
		int fd=socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
		
		strcpy(ifr.ifr_name, ifname);
		
		if(ioctl(fd, SIOCGIFINDEX, &ifr) < 0)
		{
			printe("!SIOCGIFINDEX\n");
			close(fd);
			return -1;
		}
		
		if(ip)
		{
			struct sockaddr_in sin[1];
			sin->sin_family = AF_INET;
			sin->sin_port = 0;
			sin->sin_addr.s_addr=resolv(ip);
			
			memcpy((char*)&ifr.ifr_addr, (char *) sin, sizeof(sin));
			if(ioctl(fd, SIOCSIFADDR, &ifr) < 0)
			{
				printe("!SIOCSIFADDR\n");
				close(fd);
				return -1;
			}
		}
		
		if(mask)
		{
			struct sockaddr_in sin[1];
			sin->sin_family = AF_INET;
			sin->sin_port = 0;
			sin->sin_addr.s_addr=resolv(mask);
			
			memcpy((char*)&ifr.ifr_netmask, (char *) sin, sizeof(sin));
			if(ioctl(fd, SIOCSIFNETMASK, &ifr) < 0)
			{
				printe("!SIOCSIFNETMASK\n");
			}
		}
		
		if(gateway)
		{
			struct rtentry rt[1];
			struct sockaddr_in sin[1];
			sin->sin_family = AF_INET;
			sin->sin_port = 0;
			
			memset((char *) rt, 0, sizeof(rt));
			
			sin->sin_addr.s_addr=INADDR_ANY;
			memcpy((char *) &rt->rt_dst, (char *) sin, sizeof(sin));
			sin->sin_addr.s_addr=resolv(gateway);
			memcpy((char *) &rt->rt_gateway, (char *) sin, sizeof(sin));
			sin->sin_addr.s_addr=INADDR_ANY;
			memcpy((char *) &rt->rt_genmask, (char *) sin, sizeof(sin));
			rt->rt_flags = RTF_UP | RTF_GATEWAY | RTF_STATIC;
			if(ioctl(fd, SIOCADDRT, rt) < 0)
			{
				printe("!SIOCADDRT\n");
			}
		}
		
		close(fd);
	}
	
	int max=0;
	if(max<STDIN_FILENO)
		max=STDIN_FILENO;
	if(max<fd)
		max=fd;
	max++;
	
	struct Packet
	{
		uint32_t len;
		char data[4*4096-sizeof(len)]; // enough for jumbo frame
	} __attribute__((__packed__));

	debug(4,"select\n");
	while(1)
	{
		fd_set rfds;
		struct timeval tv;
		int status;
		FD_ZERO(&rfds);
		FD_SET(STDIN_FILENO, &rfds);
		FD_SET(fd, &rfds);
		
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		status=select(max, &rfds, NULL, NULL, &tv);
		if(status>=0)
		{
			if(FD_ISSET(STDIN_FILENO,&rfds))
			{
				Packet packet;
				ssize_t done,todo,status;
				done=0;
				todo=sizeof(packet.len);
				while(done<todo)
				{
					status=read(STDIN_FILENO,&((char*)&packet.len)[done],todo-done);
					if(status<=0)
					{
						printe("!read %zd %zd!=%zd\n",status,done,todo);
						break;
					}
					done+=status;
				}
				if(done!=todo)
				{
					printe("!len %zd\n",status);
					break;
				}
				todo=ntohl(packet.len);
				if(todo>sizeof(packet.data))
				{
					printe("?len %zd\n",todo);
					break;
				}
				done=0;
				while(done<todo)
				{
					status=read(STDIN_FILENO,&packet.data[done],todo-done);
					if(status<=0)
					{
						printe("!read %zd %zd!=%zd\n",status,done,todo);
						break;
					}
					done+=status;
				}
				debug(2,"send %zd\n",todo);
				status=write(fd,packet.data,todo);
				if(status!=todo)
				{
					printe("!send %zd/%zd %d\n",status,todo,errno);
					break;
				}
			}
			if(FD_ISSET(fd,&rfds))
			{
				Packet packet;
				ssize_t done,todo;
				done=read(fd, packet.data, sizeof(packet.data));
				if(done<=0)
				{
					printe("!recv %d\n",errno);
					break;
				}
				todo=done;
				debug(2,"recv %zd\n",todo);
				packet.len=htonl(todo);
				done=write(STDOUT_FILENO,&packet,sizeof(packet.len)+todo);
				if(done!=sizeof(packet.len)+todo)
				{
					printe("!write %d\n",errno);
					break;
				}
			}
		}
		else
		if(status==0)
		{
			printe("--nodata\n");
		}
		else
		{
			printe("!select\n");
			break;
		}
	}
	close(fd);
	return 0;
}

